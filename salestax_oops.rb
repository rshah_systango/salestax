require 'csv'
require 'pry'
IND = "India"
EGY = "Egypt"
JPN = "Japan"  

INPUT_FILENAME='input.csv'
OUTPUT_FILENAME='output.csv' 

  class Product
    @sales_tax=0,@final_price=0
    def initialize (id,name,price,country)
      @id=id
      @name=name
      @price=price
      @country=country
    end  
    
    def calculate_sales_tax
      check_country_tax
    end  
    
    def check_country_tax
      if (@country==IND)
          evaluate_sales_tax_india
      elsif (@country==EGY)
            evaluate_sales_tax_egypt
      elsif (@country==JPN)
            evaluate_sales_tax_japan                
      else
            puts 'country #{@country} data not present'    
      end
    end
      
    def evaluate_sales_tax_india
        if (@price >= 100 and @price < 500)
          @sales_tax= (@price*10)/100
          evaluate_final_price_india
        elsif(@price >= 500 and @price < 1000)
          @sales_tax= (@price*15)/100
          evaluate_final_price_india
        elsif(@price >= 1000)
          @sales_tax= (@price*20)/100    
          evaluate_final_price_india  
        else
          @sales_tax=0  
          evaluate_final_price_india
        end
    end

    def evaluate_sales_tax_japan
      if(@price >= 10000)
        @sales_tax=(2*@price)/100 
        evaluate_final_price_japan
      else
        @sales_tax=0
        evaluate_final_price_japan
      end  
    end
             
    def evaluate_sales_tax_egypt
      if(@price >= 1000)
        @sales_tax=(30*@price)/100
        evaluate_final_price_egypt
      else 
        @sales_tax=0
        evaluate_final_price_egypt
      end  
    end    
    
    def evaluate_final_price_india
       @final_price=@sales_tax+@price    
    end 
    
    def evaluate_final_price_japan
        @final_price=@sales_tax+@price    
    end
    
    def evaluate_final_price_egypt
        @final_price=@sales_tax+@price    
    end

    def get_id
        return @id
    end  

    def get_price
        return @price
    end
    
    def get_country
        return @country
    end
    
    def get_name
        return @name
    end

    def get_sales_tax
        return @sales_tax
    end

    def get_final_price
        return @final_price
    end


  end

  class SalesTaxCalculator
    
    def initialize (in_name,out_name)
      @input_file_name=in_name
      @output_file_name=out_name
    end

    def process
      inp_reader=InputReader.new(@input_file_name)
      rec_contain=RecordContainer.new
      rec_initialize=RecordInitilizer.new(inp_reader,rec_contain)
      rec_process=RecordProcessor.new(rec_contain)
      out_writer=OutputWriter.new(@output_file_name,rec_contain)

      inp_reader.validate_file
      rec_initialize.assign_records
      rec_process.process_records
      out_writer.validate_file
      out_writer.write_records
    end 

  end  
  
  class InputReader
    
    def initialize (in_name)
      @input_filename=in_name
    end
      
    def validate_file
      file_existance=check_file_exist
      permission=check_permission

    end
    
    def check_file_exist
      if((File.file?(@input_filename))==true)
        puts 'file found'
        return 1
      else
        puts '#{@input_filename}file not found'
        exit 1  
      end
    end
    
    def check_permission
      if(File.readable?(@input_filename)==true)
        puts 'file readable'
        return 1
      else
        puts '#{@input_filename} file not readable'
        exit 1  
      end
    end   
    
    def read_file 
      row_counter=0
      row_arr=Array.new
        CSV.foreach(@input_filename, converters: :numeric) do |row|
            if (row_counter>=1)
              row_arr<<row                           
            end
        row_counter=row_counter+1
        end         
      return row_arr  
    end
  end

  class RecordInitilizer
    
    def initialize(in_reader,rec_container)
      @input_reader=in_reader  
      @record_container=rec_container
    end

    def assign_records
                  
      file_array=@input_reader.read_file
      file_array.each do |row|
        prod_obj=Product.new((get_id_from_row (row)),(get_name_from_row (row)),(get_price_from_row (row)),(get_country_from_row (row))) 
        @record_container.add_records(prod_obj)
      end
    end 
    

    def get_id_from_row (row)
        return row [0]
    end
          
    def get_name_from_row (row)
        return row[1]
    end
      
    def get_price_from_row (row)
        return row[2]
    end

    def get_country_from_row (row)
        return row[3]
    end
  
  end 

  class RecordContainer  
      
    @prod_array
    
    def initialize
    @prod_array ||= []
    end

    def add_records(prod_obj)
          @prod_array << prod_obj
    end

    def for_each_record()
      return @prod_array
    end  
  end

  class RecordProcessor
    @record_container
    def initialize (rec_container)
      @record_container=rec_container
    end

    def process_records
     @record_container.for_each_record.each do |product|
        product.calculate_sales_tax
     end   
    end  

  end 

  class OutputWriter 
    
    def initialize (out_name,rec_con)
      @output_file_name=out_name
      @rec_container=rec_con
    end

    def validate_file
      check_file_exist
      check_permission
    end  
    
    def check_file_exist
      if((File.file?(@output_file_name))==true)
        puts 'file found'
        return 1
      else
        puts '#{@output_filename}file not found'
        exit 1
      end    
    end

    def check_permission
      if(File.readable?(@output_file_name)==true)
        puts 'file readable'
        return 1
      else
        puts '#{@output_filename} file not readable'
        exit 1  
      end
    end
    
    def write_records
      CSV.open(@output_file_name, "a+") do |csv|
        @rec_container.for_each_record.each do |product|
          csv<<[product.get_id,product.get_name,product.get_price,product.get_country,product.get_sales_tax,product.get_final_price]
        end
      end
    end  
  
  end  

  sales_tax_cal=SalesTaxCalculator.new(INPUT_FILENAME,OUTPUT_FILENAME)
  sales_tax_cal.process