require_relative 'Product'

class RecordInitializer
   
    def initialize(input_reader)
      @input_reader=input_reader  
    end

    def populate_products
       
      file_array=@input_reader.read_file
      file_array.each do |row|
        prod_obj=Product.new((get_id_from_row (row)),(get_name_from_row (row)),(get_price_from_row (row)),(get_country_from_row (row))) 
        RecordContainer.instance.add_records(prod_obj)
      end
    end 
    
    def get_id_from_row (row)
        return row [0].to_s rescue ' '
    end
          
    def get_name_from_row (row)
        return row[1].to_s rescue ' '
    end
      
    def get_price_from_row (row)
        return row[2].to_i rescue 0
    end

    def get_country_from_row (row)
        return row[3].to_s rescue ' '
    end
  private :get_country_from_row, :get_price_from_row, :get_id_from_row, :get_name_from_row
  end 
