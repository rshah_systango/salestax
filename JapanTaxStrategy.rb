require_relative 'Product'
require_relative 'CalculateTaxStrategy'
require_relative	'StrategyFactory'
class JapanTaxStrategy < CalculateTaxStrategy
	
	def initialize (product)
		@product=product
	end

	def calculate_tax
    if (@product.get_price >= 10000)
        return sales_tax= (@product.get_price*2)/100
    else
        return sales_tax= 0  
    end
  end	

end	