require 'pry'
class FileValidator
  
  def self.check_file_exist (file_name)
    return (File.file?(file_name))
  end
    
  def self.check_read_permission (file_name)
     return (File.readable?(file_name))
  end

  def self.check_write_permission (file_name)
    return(File.writable?(file_name))
  end   
end  