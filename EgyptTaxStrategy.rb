require_relative 'Product'
require_relative 'CalculateTaxStrategy'
class EgyptTaxStrategy < CalculateTaxStrategy
	def initialize (product)
		@product=product
	end

	def calculate_tax
    if (@product.get_price >= 1000)
        return sales_tax= (@product.get_price*30)/100
    else
        return sales_tax= 0  
    end
  end	

end	
