require_relative 'InputReader'
require_relative 'RecordContainer'
require_relative 'RecordInitializer' 
require_relative 'RecordProcessor'
require_relative 'OutputWriter'    
class SalesTaxCalculator
    
    def initialize (input_file_name,output_file_name)
      @input_file_name=input_file_name
      @output_file_name=output_file_name
    end

    def process
      input_reader=InputReader.new(@input_file_name)
      record_initializer=RecordInitializer.new(input_reader)
      record_processor=RecordProcessor.new()
      output_writer=OutputWriter.new(@output_file_name)
      record_initializer.populate_products
      record_processor.process_records
      output_writer.write_records
    end 

  end