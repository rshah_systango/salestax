require_relative 'IndianTaxStrategy'
require_relative 'JapanTaxStrategy'
require_relative 'EgyptTaxStrategy'
require_relative 'Product'

class StrategyFactory

	def get_strategy (product)
    if (product.get_country==Country::IND)
      return IndianTaxStrategy.new(product)
    elsif (product.get_country==Country::JPN)
      return JapanTaxStrategy.new(product)      
    elsif (product.get_country==Country::EGY)
      return EgyptTaxStrategy.new(product)
    else
        puts 'the #{product.get_country} is not supported'
    end       
  end
end    