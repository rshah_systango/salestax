require_relative 'StrategyFactory'
require_relative 'IndianTaxStrategy'
require_relative 'JapanTaxStrategy'
require_relative 'EgyptTaxStrategy'
class Product
    
    def initialize (id,name,price,country)
      @id=id
      @name=name
      @price=price
      @country=country
      @sales_tax=0
      @final_price=0
    end  
    
    def process 
      strategy_factory= ::StrategyFactory.new()
      calculated_strategy= strategy_factory.get_strategy(self)
      set_sales_tax(calculated_strategy.calculate_tax)
      evaluate_final_price
    end
    
    def evaluate_final_price
       @final_price=@sales_tax+@price    
    end 
    
    def set_sales_tax(sales_tax)
      @sales_tax=sales_tax 
    end

    def get_id
      return @id
    end  

    def get_price
      return @price
    end
    
    def get_country
      return @country
    end
    
    def get_name
      return @name
    end

    def get_sales_tax
      return @sales_tax
    end

    def get_final_price
      return @final_price
    end
    private :evaluate_final_price  
  end
