require_relative 'SalesTaxCalculator'


INPUT_FILENAME='input.csv'
OUTPUT_FILENAME='output.csv' 

module Country
	IND = "India"
	EGY = "Egypt"
	JPN = "Japan"  
end
puts'Write the input file name'
input_file_name=$stdin.read
puts 'Write the output file name'
output_file_name=$stdin.read
sales_tax_calculator=SalesTaxCalculator.new(input_file_name=INPUT_FILENAME,output_file_name=OUTPUT_FILENAME)
sales_tax_calculator.process
