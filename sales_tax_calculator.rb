  require 'csv'
  require  'pry'
  	class Products
  		IND = "India"
  		EGY = "Egypt"
  		JPN = "Japan"
      INPUT_FILENAME='input.csv'
      OUTPUT_FILENAME='output.csv'
  		@id='nil',@name='nil',@price=0,@country='nil',@sales_tax=0,@final_price=0 	
  		
      def process_records
        @row_counter=0
        CSV.foreach(INPUT_FILENAME, converters: :numeric) do |row|
  					if (@row_counter>=1)
              get_id_from_row (row)     
              get_name_from_row (row)                  
              get_price_from_row (row)
              get_country_from_row (row)
              check_country_tax
              write_output_records
            else
              initialize_output_file        
            end

        @row_counter=@row_counter+1
    		end        	
  		end
  		
      def initialize_output_file
          CSV.open(OUTPUT_FILENAME,"wb",converters: :numeric) do |line|
              line<<["ID","Name","Price","Country","Sales_tax","Final_Price"]
          end        
      end

      def write_output_records
          CSV.open(OUTPUT_FILENAME,"a+",converters: :numeric) do |line1|
                line1<<[@id,@name,@price,@country,@sales_tax,@final_price]
          end
      end  
      def get_id_from_row (row)
          @id=row[0]
      end
          
      def get_name_from_row (row)
          @name=row[1]
      end
      
      def get_price_from_row (row)
          @price=row[2]
      end

      def get_country_from_row (row)
          @country=row[3]
      end

      def check_country_tax
  			if (@country==IND)
            evaluate_sales_tax_india
        elsif (@country==EGY)
            evaluate_sales_tax_egypt
        elsif (@country==JPN)
            evaluate_sales_tax_japan                
        else
            puts 'country #{@country} data not present'    
  		  end
      end
      
      def evaluate_sales_tax_india
        if (@price >= 100 and @price < 500)
          @sales_tax= (@price*10)/100
          evaluate_final_price_india
        elsif(@price >= 500 and @price < 1000)
          @sales_tax= (@price*15)/100
          evaluate_final_price_india
        elsif(@price >= 1000)
          @sales_tax= (@price*20)/100    
          evaluate_final_price_india  
        else
          @sales_tax=0  
          evaluate_final_price_india
        end
      end

      def evaluate_sales_tax_japan
        if(@price >= 10000)
          @sales_tax=(2*@price)/100 
          evaluate_final_price_japan
        else
          @sales_tax=0
          evaluate_final_price_japan
        end  
      end
             
  		def evaluate_sales_tax_egypt
        if(@price >= 1000)
          @sales_tax=(30*@price)/100
          evaluate_final_price_egypt
        else 
          @sales_tax=0
          evaluate_final_price_egypt
        end  
      end    
      def evaluate_final_price_india
        @final_price=@sales_tax+@price    
      end	
      def evaluate_final_price_japan
        @final_price=@sales_tax+@price    
      end
      def evaluate_final_price_egypt
        @final_price=@sales_tax+@price    
      end
    end  

  A=Products.new
  A.process_records