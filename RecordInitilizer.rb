require_relative 'Product'

class RecordInitilizer
    include Singleton
    
    def initialize(input_reader,record_container)
      @input_reader=input_reader  
      @record_container=record_container
    end

    def assign_records
       
      file_array=@input_reader.read_file
      file_array.each do |row|
        prod_obj=Product.new((get_id_from_row (row)),(get_name_from_row (row)),(get_price_from_row (row)),(get_country_from_row (row))) 
        @record_container.add_records(prod_obj)
      end
    end 
    

    def get_id_from_row (row)
        return row [0]
    end
          
    def get_name_from_row (row)
        return row[1]
    end
      
    def get_price_from_row (row)
        return row[2]
    end

    def get_country_from_row (row)
        return row[3]
    end
  
  end 
