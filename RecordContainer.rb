require 'singleton'
class RecordContainer  

 include Singleton
    def initialize
      @products_array ||= []
    end

    def add_records(product_obj)
      @products_array << product_obj
    end

    def get_record_array()
      @products_array
    end  
end
