require_relative'Product'
class RecordProcessor
    def process_records
      RecordContainer.instance.get_record_array.each do |product|
        product.process
      end   
    end  
end 
