require_relative 'Product'
require_relative 'CalculateTaxStrategy'
require_relative 'StrategyFactory'
class IndianTaxStrategy < CalculateTaxStrategy
	def initialize (product)
		@product=product
	end

	def calculate_tax
    if (@product.get_price >= 100 and @product.get_price < 500)
          return sales_tax= (@product.get_price*10)/100
        elsif(@product.get_price >= 500 and @product.get_price < 1000)
          return sales_tax= (@product.get_price*15)/100
        elsif(@product.get_price >= 1000)
          return sales_tax= (@product.get_price*20)/100
        else
          return sales_tax= 0  
    end
  end	
end	