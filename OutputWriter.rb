require 'csv'
require 'pry'
require_relative 'FileValidator'

class OutputWriter 
    def initialize (output_file_name)
      @output_file_name=output_file_name
      if (FileValidator.check_file_exist(@output_file_name) and FileValidator.check_write_permission(@output_file_name))
        puts 'file is writable'
      else
        raise '#{@output_file_name} not found'
      end    
    end
     
    def write_records
      CSV.open(@output_file_name, "a+") do |csv|
       RecordContainer.instance.get_record_array.each do |product|
          csv<<[product.get_id,product.get_name,product.get_price,product.get_country,product.get_sales_tax,product.get_final_price]
        end
      end
    end  
  
  end  
